const express = require('express');
const {
    createTaskList, getTaskLists, getTaskList, updateTaskList, deleteTaskList
} = require('../controllers/taskListController');
const { authenticate } = require('../middleware/auth');

const router = express.Router();

router.post('/', authenticate, createTaskList);
router.get('/', authenticate, getTaskLists);
router.get('/:id', authenticate, getTaskList);
router.put('/:id', authenticate, updateTaskList);
router.delete('/:id', authenticate, deleteTaskList);

module.exports = router;
