const Task = require('../models/Task');

exports.createTask = async (req, res) => {
    const { taskListId, description, details, dueDate } = req.body;
    try {
        const newTask = new Task({ taskListId, description, details, dueDate });
        await newTask.save();
        res.status(201).json(newTask);
    } catch (error) {
        res.status(500).json({ error: error.message });
    }
};

exports.getTasks = async (req, res) => {
    const { taskListId } = req.query;
    try {
        const tasks = await Task.find({ taskListId });
        res.json(tasks);
    } catch (error) {
        res.status(500).json({ error: error.message });
    }
};

exports.getTask = async (req, res) => {
    const { id } = req.params;
    try {
        const task = await Task.findById(id);
        if (!task) return res.status(404).json({ message: 'Task not found' });
        res.json(task);
    } catch (error) {
        res.status(500).json({ error: error.message });
    }
};

exports.updateTask = async (req, res) => {
    const { id } = req.params;
    const { description, details, dueDate, completed } = req.body;
    try {
        const updatedTask = await Task.findByIdAndUpdate(id, {
            description, details, dueDate, completed
        }, { new: true });
        res.json(updatedTask);
    } catch (error) {
        res.status(500).json({ error: error.message });
    }
};

exports.deleteTask = async (req, res) => {
    const { id } = req.params;
    try {
        await Task.findByIdAndDelete(id);
        res.status(204).send();
    } catch (error) {
        res.status(500).json({ error: error.message });
    }
};
