const TaskList = require('../models/TaskList');
const Task = require('../models/Task');

exports.createTaskList = async (req, res) => {
    const { name } = req.body;
    const userId = req.user.id; // ID de l'utilisateur authentifié
    try {
        const newTaskList = new TaskList({ name, userId });
        await newTaskList.save();
        res.status(201).json(newTaskList);
    } catch (error) {
        res.status(500).json({ error: error.message });
    }
};

exports.getTaskLists = async (req, res) => {
    try {
        const taskLists = await TaskList.find({ userId: req.user.id });
        res.json(taskLists);
    } catch (error) {
        res.status(500).json({ error: error.message });
    }
};

exports.getTaskList = async (req, res) => {
    const { id } = req.params;
    try {
        const taskList = await TaskList.findById(id);
        if (!taskList) return res.status(404).json({ message: 'Task list not found' });
        res.json(taskList);
    } catch (error) {
        res.status(500).json({ error: error.message });
    }
};

exports.updateTaskList = async (req, res) => {
    const { id } = req.params;
    const { name } = req.body;
    try {
        const updatedTaskList = await TaskList.findByIdAndUpdate(id, { name }, { new: true });
        res.json(updatedTaskList);
    } catch (error) {
        res.status(500).json({ error: error.message });
    }
};

exports.deleteTaskList = async (req, res) => {
    const { id } = req.params;
    try {
        await Task.deleteMany({ taskListId: id }); // Supprime toutes les tâches liées
        await TaskList.findByIdAndDelete(id);
        res.status(204).send();
    } catch (error) {
        res.status(500).json({ error: error.message });
    }
};