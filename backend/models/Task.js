const mongoose = require('mongoose');

const taskSchema = new mongoose.Schema({
    taskListId: { type: mongoose.Schema.Types.ObjectId, ref: 'TaskList', required: true },
    description: { type: String, required: true },
    details: { type: String, default: '' },
    dueDate: { type: Date, required: true },
    completed: { type: Boolean, default: false }
});

const Task = mongoose.model('Task', taskSchema);
module.exports = Task;